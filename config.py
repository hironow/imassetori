# -*- coding: utf-8 -*-

import os
from sqlalchemy import create_engine

# Content
CONTENT_TITLE = 'THE IDOLM@STER'
CONTENT_TITLE_ABBR = 'IM@S'
# PEOPLE = 'プロデューサー'
SPREADSHEET = 'imas-setori'
AMAZON_ID = 'imas-setori-22'
AFFILIATIONS = {'765': '765PRO'}
AFFILIATION_DEFAULT = '765'

# Statement for enabling the development environment
DEBUG = False

# Define the application directory
BASE_DIR = os.path.abspath(os.path.dirname(__file__))

# Define the database - we are working with
# SQLite for this example
REDIS_URL = os.getenv('REDISCLOUD_URL', None)
ENGINE = create_engine(os.getenv('DATABASE_URL', 'sqlite:///imas.sqlite'))


# Application threads. A common general assumption is
# using 2 per available processor cores - to handle
# incoming requests using one and performing background
# operations using the other.
THREADS_PER_PAGE = 2

# Enable protection agains *Cross-site Request Forgery (CSRF)*
CSRF_ENABLED = True

# Use a secure, unique and absolutely secret key for
# signing the data.
CSRF_SESSION_KEY = "secret"

# Secret key for signing cookies
SECRET_KEY = "secret"
