# -*- coding: utf-8 -*-

from app import application
import os

port = int(os.getenv('PORT', 5000))
application.run(host='0.0.0.0', port=port)
