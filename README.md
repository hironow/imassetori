# ContentSetori


## git 管理

サブモジュール（app/setori）を扱うために subtree コマンドを使う。
`--squash` で取り込む履歴を一つにまとめることができる。

```bash
# メインモジュール git 管理開始
$ git init
$ git add .gitignore
$ git commit -m 'initial commit, with .gitignore'

# サブモジュールを subtree で追加
$ git remote add -f setori git@bitbucket.org:hironow/setori.git
$ git subtree add --prefix app/setori setori master --squash

# メイン/サブモジュールの変更を commit/push
$ git add .
$ git commit -m 'edit'
$ git push origin master  # メインモジュール
$ git subtree push --prefix app/setori setori master  # サブモジュール

# サブモジュールの変更を fetch
$ git fetch setori

# サブモジュールの変更を pull
$ git subtree pull --prefix app/setori setori master --squash
```


## python 環境

仮想環境で行い、サーバは `python run.py` か `foreman start` で起動する。

```bash
# virtualenv 作成/開始
$ pyenv virtualenv 3.4.2 setori
$ pyenv activate setori

# パッケージ追加
$ pip install --requirement requirements.txt

# パッケージ内容を書き出し
$ pip freeze > requirements.txt

# サーバ起動
$ foreman start

# virtualenv 終了
$ pyenv deactivate
```


## heroku 展開

デプロイ後のデータベース更新には `heroku run` か 外部からの `DATABASE_URL` 参照で行う。

```bash
# Heroku アプリ作成
$ heroku login
$ heroku create

# 変数を設定
$ heroku config:set GOOGLE_APPLICATION_CREDENTIALS=xxxxxxxxxx.json
$ heroku config:set WEB_CONCURRENCY=3

# アドオンを追加
$ heroku addons:create heroku-postgresql:hobby-dev
$ heroku addons:create rediscloud:30
$ heroku addons:create newrelic
$ heroku addons:create process-scheduler
$ heroku addons:create papertrail
$ heroku addons:create logentries

# リポジトリ更新
$ git add .
$ git commit -m 'edit'

# デプロイ
$ git push heroku master

# データベース更新
$ heroku run python createdb.py
$ python createdb.py  # DATABASE は HEROKU_DATABASE_URL を指定（外部から可能）
```


## データベース更新手順

1. Google spreadsheet の各ワークシートを pandas.DataFrame として取得
2. イベントの part 、リリースの disc を replace (`str` '' ->  `int` 1)

    > *ここに `str '-'` によるキャラクターなし/役者なし or 役者のみの replace*

3. イベントとリリースの参考URLの title を `url_title` として取得
4. イベントとリリースのASINより `amazon_url` `image_url` を生成
5. `to_sql()` でデータベースに保存
6. イベントのセットリストのキャラクター、リリースのトラックリストの役者、それぞれをモデルに合わせて変換
7. 6.を`to_sql()` でデータベースに再保存
8. `name_kana` があるデータベースのカラムがすべてカナか、 `name_alphabet` があるデータベースのカラムがすべて英字か、それぞれチェック
9. イベントのセットリストとリリースのトラックリストの `song` `song_version` `type` がそれぞれの `order` に対応しているかチェック

- 参考URLの取得はチェックが終わった **さいご** にすべき
- `affiliation` のチェックもいれるべき
