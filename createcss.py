# -*- coding: utf-8 -*-

from jinja2 import FileSystemLoader
from jinja2.environment import Environment

from app.setori import df_char
from app.setori.models import get_models, Character
import colorsys


def hex_to_rgb(hex):
    v = hex.lstrip('#')
    lv = len(v)
    return tuple(int(v[i:i+lv//3], 16) for i in range(0, lv, lv//3))


env = Environment()
env.loader = FileSystemLoader('app/templates/setori/')
template = env.get_template('colors.css')

models = get_models(df_char, Character, ['name'])
[setattr(model, 'color', hex_to_rgb(model.color)) for model in models]

white = '#FFFFFF'
black = '#333333'
white_luminance = colorsys.rgb_to_yiq(*hex_to_rgb(white))[0]
black_luminance = colorsys.rgb_to_yiq(*hex_to_rgb(black))[0]
for model in models:
    luminance = colorsys.rgb_to_yiq(*model.color)[0]
    if white_luminance - luminance < 125:
        setattr(model, 'font_color', black)
    else:
        setattr(model, 'font_color', white)

r = template.render(models=models)

with open('app/static/css/colors.css', 'w') as f:
    f.write(r)
