# -*- coding: utf-8 -*-

from lxml import etree
import requests
try:
    import cchardet as chardet
except ImportError:
    import chardet
import json
import gspread
from oauth2client.client import GoogleCredentials
import pandas as pd

json_key = json.load(open('setori-184a4e380e99.json'))
scope = ['https://spreadsheets.google.com/feeds']
credentials = GoogleCredentials.get_application_default()
credentials = credentials.create_scoped(scope)


class SheetNotFound(Exception):
    pass


def get_spreadsheet(name):
    """ Get Google spreadsheet.

        :param str name: spreadsheet name
        :return: list of googel spreadsheet worksheets
        :rtype: list
    """
    gc = gspread.authorize(credentials)
    worksheets = gc.open(name).worksheets()
    return worksheets


def get_worksheet(worksheets, name):
    """ Get Google spreadsheet worksheet.

        :param list worksheets: list of worksheets, [gspread.worksheet,
            gspread.worksheet, ...]
        :param str name: worksheet name
        :return: google spreadsheet worksheet pandas dataframe
        :rtype: pandas.DataFrame
    """
    hit = False
    for worksheet in worksheets:
        if worksheet.title == name:
            df = convert_sheet(worksheet)
            hit = True
    else:
        if not hit:
            raise SheetNotFound('Worksheet %s is not found' % name)
    return df


def convert_sheet(worksheet):
    """ Convert Google spreadsheet list of list to pandas dataframe.

        :param worksheet: gspread.worksheet
        :return: pandas dataframe
        :rtype: pandas.DataFrame
    """
    lls = [[tryint(x) for x in ls] for ls in worksheet.get_all_values()]
    return pd.DataFrame(lls[1:], columns=lls[0])


def tryint(s):
    try:
        return int(s)
    except:
        return s


def get_website_title(url):
    """ Get website title.

        :param str url: url
        :return: website title
        :rtype: str
    """
    try:
        response = requests.get(url)
    except Exception as e:
        print("Error in '%s'\n%s" % (url, e))
        return ''
    encoding = chardet.detect(response.content)['encoding']
    tree = etree.HTML(response.content,
                      parser=etree.HTMLParser(encoding=encoding))
    title_elements = tree.xpath('//title')
    if title_elements:
        title = title_elements[0].text.strip()
    else:
        title = ''
    if url != response.url:
        print('not equal response url: %s' % url)
    return title


def convert_event_characters():
    from app.setori import df_song2actr2event
    from app.setori.models import Event, Actor, Character
    from app.setori.exceptions import InvalidEventModel

    # event has actor
    for idx, row in df_song2actr2event.iterrows():
        if row['actor'] == '':
            print("ooo actor '' has character '{0}'".format(row['character']))
            continue
        event = Event(row['event'])
        # part number is used as lists index
        affiliations = event.parts[row['part_number']-1]['affiliations']
        if row['character'] != '':
            # TODO: if  == '-': set null character
            # use in place value, but check correct value
            character = Character(row['character'])
            character.set_actors(affiliations=affiliations)
            if not row['actor'] in [a.name for a in character.actors]:
                raise InvalidEventModel(
                    'Character {0} is bad input'.format(row['character']))
        elif row['character'] == '':
            # use in replace value
            actor = Actor(row['actor'])
            actor.set_characters(affiliations=affiliations, date=event.date)
            if len(actor.characters) > 1:
                df_song2actr2event.set_value(index=idx, col='character',
                                             value=actor.characters[0].name)
                for c in actor.characters[1:]:
                    r = row.copy().set_value(label='character', value=c.name)
                    df_song2actr2event = df_song2actr2event.append(r)
            elif len(actor.characters) == 1:
                df_song2actr2event.set_value(index=idx, col='character',
                                             value=actor.characters[0].name)
    df_song2actr2event = df_song2actr2event.sort_index(
        by=['event', 'part_number', 'order', 'actor'],
        ascending=[True, True, True, True])
    df_song2actr2event['index'] = range(0, len(df_song2actr2event))
    df_song2actr2event.set_index(['index'])
    return df_song2actr2event


def convert_release_actors():
    from app.setori import df_song2char2release
    from app.setori.models import Release, Actor, Character
    from app.setori.exceptions import InvalidReleaseModel

    # release has character
    for idx, row in df_song2char2release.iterrows():
        if row['character'] == '':
            print("ooo character: '' has actor '{0}'".format(row['actor']))
            if row['actor'] == '':
                print('karaoke')
            continue
        release = Release(row['release'])
        # disc number is used as lists index
        affiliations = release.discs[row['disc_number']-1]['affiliations']
        if row['actor'] != '':
            # use in place value, but check correct value
            actor = Actor(row['actor'])
            actor.set_characters(affiliations=affiliations)
            if not row['character'] in [c.name for c in actor.characters]:
                raise InvalidReleaseModel(
                    'Actor {0} is bad input'.format(row['actor']))
        elif row['actor'] == '':
            # use in replace value via date
            character = Character(row['character'])
            character.set_actors(affiliations=affiliations, date=release.date)
            if len(character.actors) > 1:
                df_song2char2release.set_value(index=idx, col='actor',
                                               value=character.actors[0].name)
                for a in character.actors[1:]:
                    r = row.copy().set_value(label='actor', value=a.name)
                    df_song2char2release = df_song2char2release.append(r)
            elif len(character.actors) == 1:
                df_song2char2release.set_value(index=idx, col='actor',
                                               value=character.actors[0].name)
    df_song2char2release = df_song2char2release.sort_index(
        by=['release', 'disc_number', 'order', 'character'],
        ascending=[True, True, True, True])
    df_song2char2release['index'] = range(0, len(df_song2char2release))
    df_song2char2release.set_index(['index'])
    return df_song2char2release


def getkata(s):
    c = [char for char in s if '\u30A0' <= char <= '\u30FF']
    return ''.join(c)


def iskata(s):
    if len(s) == 0:
        return False
    elif len(s) == len(getkata(s)):
        return True
    else:
        return False

    # Corrected check
    for df, name in dfs:
        if not all(df['name_kana'].map(lambda x: iskata(x))):
            print('xxx bad kana df {0}'.format(name))
        if not all(df['name_alphabet'].map(lambda x: x.isalnum())):
            print('xxx bad alphabet df {0}'.format(name))


def iskana_column(df):
    if all(df['name_kana'].map(lambda x: iskata(x.replace(' ', '')))):
        return True
    else:
        return False


def isalnum_column(df):
    if all(df['name_alphabet'].map(lambda x: x.replace(' ', '').isalnum())):
        return True
    else:
        return False


if __name__ == '__main__':
    from config import SPREADSHEET, CONTENT_TITLE, AMAZON_ID, ENGINE

    engine = ENGINE

    sheets = get_spreadsheet(name=SPREADSHEET)
    # check TODO: not print, DO logger
    print('--> %s via %s (amz: %s)' % (
        CONTENT_TITLE, SPREADSHEET, AMAZON_ID))

    # Get worksheets and convert for pandas.DataFrame
    char = get_worksheet(sheets, 'character')
    actr = get_worksheet(sheets, 'actor')
    char2actr = get_worksheet(sheets, 'character-actor')

    song = get_worksheet(sheets, 'song')

    event = get_worksheet(sheets, 'event')
    song2actr2event = get_worksheet(sheets, 'song-actor-event')
    slsrc = get_worksheet(sheets, 'setlist-source')

    release = get_worksheet(sheets, 'release')
    song2char2release = get_worksheet(sheets, 'song-character-release')
    tlsrc = get_worksheet(sheets, 'tracklist-source')

    # Convert part/disc number
    event['part_number'] = event['part_number'].replace('', 1)
    song2actr2event['part_number'] = song2actr2event[
        'part_number'].replace('', 1)
    release['disc_number'] = release['disc_number'].replace('', 1)
    song2char2release['disc_number'] = song2char2release[
        'disc_number'].replace('', 1)

    # Get website title
    slsrc['url_title'] = [get_website_title(url) for url in slsrc['url']]
    tlsrc['url_title'] = [get_website_title(url) for url in tlsrc['url']]

    # Convert for SQL
    dfs = [(char, 'char'), (actr, 'actr'), (song, 'song'),
           (event, 'event'), (release, 'release')]
    relations = [(char2actr, 'char2actr'),
                 (song2actr2event, 'song2actr2event'),
                 (song2char2release, 'song2char2release'),
                 (slsrc, 'slsrc'), (tlsrc, 'tlsrc')]
    for df, name in dfs + relations:
        df.to_sql(name, engine, if_exists='replace')
        print('... %s saved' % name)

    # Modeled convert for SQL
    df_song2actr2event = convert_event_characters()
    df_song2char2release = convert_release_actors()
    relations = [(df_song2actr2event, 'song2actr2event',
                  ['event', 'part_number', 'order']),
                 (df_song2char2release, 'song2char2release',
                  ['release', 'disc_number', 'order'])]
    for df, name, _ in relations:
        df.to_sql(name, engine, if_exists='replace')
        print('___ %s modeled converted' % name)

    # Corrected check
    for df, name in dfs:
        if not iskana_column(df):
            print("xxx all kana data via '{0}' are not kana".format(name))
        if not isalnum_column(df):
            print("xxx all alnum data via '{0}' are not alnum".format(name))

    columns = ['song', 'song_version', 'type']
    for df, name, groupby_columns in relations:
        for column in columns:
            if not all(df.groupby(groupby_columns)[column].nunique() == 1):
                print('xxx bad {0} df {1}'.format(column, name))
