# -*- coding: utf-8 -*-

from flask import Flask, render_template
from flask.ext.cache import Cache
from requests.utils import urlparse


# Define the WSGI application object
application = Flask(__name__)

# Configurations
application.config.from_object('config')

redis_url = application.config['REDIS_URL']
if redis_url:
    url = urlparse(redis_url)
    cache = Cache(application, config={
        'CACHE_TYPE': 'redis',
        'CACHE_KEY_PREFIX': 'fcache',
        'CACHE_REDIS_HOST': url.hostname,
        'CACHE_REDIS_PORT': url.port,
        'CACHE_REDIS_PASSWORD': url.password
    })
else:
    cache = Cache(application, config={
        'CACHE_TYPE': 'redis',
        'CACHE_KEY_PREFIX': 'fcache',
        'CACHE_REDIS_HOST': 'localhost',
        'CACHE_REDIS_PORT': '6379',
        'CACHE_REDIS_URL': 'redis://localhost:6379'
    })

# Define the database object which is imported by modules and controllers
engine = application.config['ENGINE']

application.jinja_env.globals['zip'] = zip
application.jinja_env.globals[
    'content_title'] = application.config['CONTENT_TITLE']
application.jinja_env.globals[
    'content_title_abbr'] = application.config['CONTENT_TITLE_ABBR']
# p = application.config['PEOPLE']
# a = application.config['AFFILIATIONS']

# Import a module / component using its blueprint handler variable
from .setori.controllers import setori
from .setori.api import api
# Register blueprint(s)
application.register_blueprint(setori)
application.register_blueprint(api)

# Build the database:
# This will create the database file using SQLAlchemy
# db.create_all()


@application.errorhandler(400)
def bad_request_error(error):
    # TODO: request header json -> json 400!
    print(error)
    return render_template('setori/400.html'), 400


@application.errorhandler(404)
def not_found_error(error):
    # TODO: request header json -> json 404!
    print(error)
    return render_template('setori/404.html'), 404
