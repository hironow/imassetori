'use strict';

function filterBySelector() {
  // セレクタでmixをフィルタ
  // 曲、役者、キャラクター、イベント、リリースの単体ページで使用

  var $filterSelect = $('#filter-select');
  var $container = $('#main #selector');

  // スマートフォンは端末のセレクタ表示使用
  var regex = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i;

  if (regex.test(navigator.userAgent)) {
    $('.selectpicker').selectpicker({
      'mobile': true,
      'showSubtext': false
    });
  } else {
    $('.selectpicker').selectpicker({
      'showSubtext': false
    });
  }

  // mixitupでフィルタ
  $container.mixItUp({
    layout: {
      display: 'block'
    },
    animation: {
      enable: false
    },
    callbacks: {
      onMixEnd: function(state) {
        console.log('MixItUp filtered by selector... ' + state.activeFilter);

        $('.filtered').removeClass('active');
        $('.filtered' + state.activeFilter).addClass('active');
      }
    }
  });

  $filterSelect.on('change', function() {
    $container.mixItUp('filter', this.value);
  });
}

function filterByDateAndString() {
  // 日付でmixをフィルタ
  // イベントとリリースの一覧ページで使用

  var $container = $('#main #datestring');

  // mixitupでフィルタ
  $container.mixItUp({
    layout: {
      display: 'block'
    },
    animation: {
      enable: false
    },
    callbacks: {
      onMixEnd: function(state) {
        console.log('MixItUp filtered by date and string... ' + state.activeFilter);

        $('.counter').text(state.totalShow +'件');
      }
    }
  });

  var today = moment();

  // 日付が設定された要素をリスト化
  var dateList = _.map($('#main #datestring .mix'), function(o) {
    var dataDate = $(o).data('date')
    var date = (dataDate === '') ? null : moment(dataDate, 'YYYYMMDD');
    // console.log(date, date > today || date === null);
    if (date > today || date === null) {
      return;
    } else {
      return date;
    }
  });

  var minDate = _.min(dateList).format('X');
  // var maxDate = _.max(dateList).format('X');
  var maxDate = today.format('X');
  $('.date-slider').ionRangeSlider({
    type: 'double',
    min: +minDate,
    max: +maxDate,
    from: +minDate,
    to: +maxDate,
    grid: true,
    grid_num: 3,
    force_edges: true,
    prettify: function(num) {
      var m = moment(num, 'X').locale('ja');
      return m.format('YYYY.M.D');
    },
    onChange: function(data) {
      // すべての要素のフィルタのactive切り替え
      $('#main #datestring .filter[data-filter="all"]').removeClass('active')

      // 未来の要素のフィルタのactiveを削除
      $('.filter-future').removeClass('active')

      var selectObj = $('#main #datestring .mix').filter(function(i, e) {
        var dataDate = $(e).data('date')
        var unixDate = (dataDate === '') ? null : moment(dataDate, 'YYYYMMDD').format('X');
        return (unixDate >= data.from && unixDate <= data.to);
      });
      $container.mixItUp('filter', selectObj);
    }
  });

  var slider = $('.date-slider').data('ionRangeSlider');

  $('#main #datestring .filter[data-filter="all"]').on('click', function() {
    // すべての要素のフィルタのactive切り替え
    $('.filter-future').removeClass('active')
  });

  // 未来の要素をmixitupでfilter
  $('.filter-future').on('click', function() {
    if (!$(this).hasClass('active')) {
      $(this).addClass('active');
      $('#main #datestring .filter[data-filter="all"]').removeClass('active')

      var selectObj = $('#main #datestring .mix').filter(function(i, e) {
        var dataDate = $(e).data('date')
        var date = (dataDate === '') ? null : moment(dataDate, 'YYYYMMDD');
        return (date > today || date === null);
      });
      $container.mixItUp('filter', selectObj);
    }
  });

  // 検索フォームの名称でフィルター
  $('#search-filter').japaneseInputChange(250, function() {
    var searchString = $(this).val().toLowerCase();

    var $matching = $();
    $('.mix').each(function() {
      var $this = $(this);
      if ($this.find('.cell.header .filtered').text().toLowerCase().indexOf(searchString) > -1) {
        $matching = $matching.add(this);
      }
    });
    // 未来の要素のフィルタのactiveを削除
    $('.filter-future').removeClass('active')

    $container.mixItUp('filter', $matching);
  });
}

function filterBlockByString() {
  // 文字列でmixをフィルタ
  // 曲の一覧ページで使用
  var $container = $('#main #stringblock');
  $container.mixItUp({
    layout: {
      display: 'block'
    },
    animation: {
      enable: false
    },
    callbacks: {
      onMixEnd: function(state) {
        console.log('MixItUp filtered by string block... ' + state.activeFilter);

        $('.counter').text(state.totalShow +'件');
      }
    }
  });

  $('#search-filter').japaneseInputChange(250, function() {
    var searchString = $(this).val().toLowerCase();

    var $matching = $();
    $('.mix').each(function() {
      var $this = $(this);
      if ($this.find('.cell.header').text().toLowerCase().indexOf(searchString) > -1) {
        $matching = $matching.add(this);
      }
    });
    $container.mixItUp('filter', $matching);
  });
}

function filterInlineBlockByString() {
  // 文字列でmixをフィルタ
  // 役者、キャラクターの一覧ページで使用
  var $listContainer = $('#main #stringinlineblock');
  $listContainer.mixItUp({
    layout: {
      display: 'inline-block'
    },
    animation: {
      enable: false
    },
    callbacks: {
      onMixEnd: function(state) {
        console.log('MixItUp filtered by string inlineblock... ' + state.activeFilter);

        $('.counter').text(state.totalShow +'件');
      }
    }
  });

  $('#search-filter').japaneseInputChange(250, function() {
    var searchString = $(this).val().toLowerCase();

    var $matching = $();
    $('.mix').each(function() {
      var $this = $(this);
      if ($this.text().toLowerCase().indexOf(searchString) > -1) {
        $matching = $matching.add(this);
      }
    });
    $listContainer.mixItUp('filter', $matching);
  });
}


// constructs the suggestion song, actor, character
var song = new Bloodhound({
  datumTokenizer: function(d) {
    return Bloodhound.tokenizers.whitespace(d.tokens.join(' '));
  },
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  limit: 15,
  remote: window.location.origin + '/api/v1/songs?q=%QUERY'
});

var actor = new Bloodhound({
  datumTokenizer: function(d) {
    return Bloodhound.tokenizers.whitespace(d.tokens.join(' '));
  },
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  limit: 15,
  remote: window.location.origin + '/api/v1/actors?q=%QUERY'
});

var character = new Bloodhound({
  datumTokenizer: function(d) {
    return Bloodhound.tokenizers.whitespace(d.tokens.join(' '));
  },
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  limit: 15,
  remote: window.location.origin + '/api/v1/characters?q=%QUERY'
});

// kicks off the loading/processing
var promise = song.initialize();
promise
.done(function() { console.log('Bloodhound initialize... song success!'); })
.fail(function() { console.log('Bloodhound initialize... song error!'); });

var promise = actor.initialize();
promise
.done(function() { console.log('Bloodhound initialize... actor success!'); })
.fail(function() { console.log('Bloodhound initialize... actor error!'); });

var promise = character.initialize();
promise
.done(function() { console.log('Bloodhound initialize... character success!'); })
.fail(function() { console.log('Bloodhound initialize... character error!'); });


$(document).ready(function(e){

  // 初期のモデルをhiddenから取得して自動推定を選択する
  var param = $('.input-group .input-group-select-val').val();
  $('#bloodhound .typeahead').typeahead('destroy');

  if (param === 'song') {
    $('#bloodhound .typeahead').typeahead({
      hint: true,
      highlight: true,
      minLength: 1
    },
    {
      name: param,
      displayKey: 'value',
      source: song.ttAdapter()
    });
  } else if (param === 'actor') {
    $('#bloodhound .typeahead').typeahead({
      hint: true,
      highlight: true,
      minLength: 1
    },
    {
      name: param,
      displayKey: 'value',
      source: actor.ttAdapter()
    });
  } else if (param === 'character') {
    $('#bloodhound .typeahead').typeahead({
      hint: true,
      highlight: true,
      minLength: 1
    },
    {
      name: param,
      displayKey: 'value',
      source: character.ttAdapter()
    });
  }
  console.log('Autocomplete for... ', param);

  // モデルを選択したときのイベントで自動推定を切り替え
  $('.input-group-select .dropdown-menu').find('a').click(function(e) {
    e.preventDefault();
    var param = $(this).attr('href').replace('#','');
    var concept = $(this).text();
    $('.input-group-select span.concept').text(concept);
    $('.input-group .input-group-select-val').val(param);

    $('#bloodhound .typeahead').typeahead('destroy');

    if (param === 'song') {
      $('#bloodhound .typeahead').typeahead({
        hint: true,
        highlight: true,
        minLength: 1
      },
      {
        name: param,
        displayKey: 'value',
        source: song.ttAdapter()
      });
    } else if (param === 'actor') {
      $('#bloodhound .typeahead').typeahead({
        hint: true,
        highlight: true,
        minLength: 1
      },
      {
        name: param,
        displayKey: 'value',
        source: actor.ttAdapter()
      });
    } else if (param === 'character') {
      $('#bloodhound .typeahead').typeahead({
        hint: true,
        highlight: true,
        minLength: 1
      },
      {
        name: param,
        displayKey: 'value',
        source: character.ttAdapter()
      });
    }
    console.log('Autocomplete change to... ', param, '(', concept, ')');
  });

  if (document.getElementById('selector')) {
    console.log('main #selector content...');
    filterBySelector();
  } else if (document.getElementById('datestring')) {
    console.log('main #datestring content...');
    filterByDateAndString();
  } else if (document.getElementById('stringblock')) {
    console.log('main #stringblock content...');
    filterBlockByString();
  } else if (document.getElementById('stringinlineblock')) {
    console.log('main #stringinlineblock content...');
    filterInlineBlockByString();
  };
});
