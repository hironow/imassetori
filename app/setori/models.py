# -*- coding: utf-8 -*-

"""
setori.models
~~~~~~~~~~~~~

This module contains the set of setori's models.

"""

from . import (df_song,
               df_actr,
               df_char,
               df_event,
               df_release,
               df_char2actr,
               df_song2actr2event,
               df_song2char2release,
               df_slsrc,
               df_tlsrc)
from .exceptions import (InvalidSongModel,
                         InvalidActorModel,
                         InvalidCharacterModel,
                         InvalidEventModel,
                         InvalidReleaseModel,
                         InvalidSetListModel,
                         InvalidTrackListModel)
from pandas.tslib import Timestamp


def and_search(keywords, df, column='name', arg=lambda x: x):
    if len(keywords) == 0:
        return df
    keyword = keywords.pop()
    result = df[df[column].map(arg).str.contains(keyword)]
    return result if len(result) == 0 else and_search(keywords, result,
                                                      column, arg)


def or_search(keywords, df, column='name', arg=lambda x: x):
    if len(keywords) == 0:
        return df
    keyword = '|'.join(keywords)
    return df[df[column].map(arg).str.contains(keyword)]


def get_model(df, model, arg_columns):
    """Modelを1つだけ取得する。

    :param df: pandas DataFrame
    :param model: Model
    :param arg_columns: Modelのinitに必要なcolumnのlist
    :type arg_columns: list
    """
    ms = get_models(df, model, arg_columns, sort=False)
    if len(ms) == 1:
        return ms[0]
    else:
        # 取得出来たModelが複数ある
        raise ValueError('Bad model')


def get_models(df, model, arg_columns, sort=True, sort_by='name'):
    """Modelを複数取得する。

    :param df: pandas DataFrame
    :param model: Model
    :param arg_columns: Modelのinitに必要なcolumnのlist
    :param sort: Modelのsortを行なうかどうか
    :type sort: bool
    :param sort_by: sortを行なうcolumn名
    :type sort_by: str
    """

    # columnがすべて同じ値である重複したrowを削除
    df = df.drop_duplicates(arg_columns)

    # 各columnをtupleで取り出してModel化
    ms = [model(*t) for t in df[arg_columns].itertuples(index=False)]

    if sort and (len(ms) > 1):
        # attrによってsortを行なう
        ms = sorted(ms, key=lambda x: getattr(x, sort_by))
    return ms


def get_relational_actor_models(df, sort=True, sort_by='name_kana',
                                duplicate_columns=['character', 'actor'],
                                group_by='group'):
    if group_by in duplicate_columns:
        duplicate_columns.remove(group_by)
        grouped_actors = {}
        for by, g in df.groupby([group_by]):
            grouped_actors[by] = get_relational_actor_models(
                g, sort=sort, sort_by=sort_by,
                duplicate_columns=duplicate_columns)
        return grouped_actors

    df = df.drop_duplicates(duplicate_columns)
    actors = []
    for by, g in df.groupby('actor',
                            as_index=False, sort=False, group_keys=False):
        actor = Actor(by)
        actor.characters = get_models(g, Character, ['character'],
                                      sort=sort, sort_by=sort_by)
        actors.append(actor)
    if sort and (len(actors) > 1):
        actors = sorted(actors, key=lambda x: getattr(x, sort_by))
    return actors


def get_relational_character_models(df, sort=True, sort_by='name_kana',
                                    duplicate_columns=['character', 'actor'],
                                    group_by='group'):
    if group_by in duplicate_columns:
        duplicate_columns.remove(group_by)
        grouped_characters = {}
        for by, g in df.groupby([group_by]):
            grouped_characters[by] = get_relational_character_models(
                g, sort=sort, sort_by=sort_by,
                duplicate_columns=duplicate_columns)
        return grouped_characters

    df = df.drop_duplicates(duplicate_columns)
    characters = []
    for by, g in df.groupby('character',
                            as_index=False, sort=False, group_keys=False):
        try:
            character = Character(by)
            character.actors = get_models(g, Actor, ['actor'],
                                          sort=sort, sort_by=sort_by)
            characters.append(character)
        except InvalidCharacterModel:
            # キャラクターのない場合に対応
            continue
    if sort and (len(characters) > 1):
        characters = sorted(characters, key=lambda x: getattr(x, sort_by))
    return characters


def search_models(q, df, model, boolean=None):
    keywords = q.split()
    boolean = BooleanType(boolean).v
    if boolean == 'and':
        return get_models(and_search(keywords, df), model, ['name'])
    elif boolean == 'or':
        return get_models(or_search(keywords, df), model, ['name'])


class BooleanType(object):
    accepts = ['and', 'or']
    default = 'and'

    def __init__(self, param):
        if param in self.accepts:
            self.v = param
        else:
            self.v = self.default

    def __repr__(self):
        return self.v


class AffiliationType(object):
    # TODO: configから取得する
    accepts = ['765', '876', '961', '765p', '346p',
               'cg', '346', 'ml', 'xe', '315', 'pu', 'ofa']
    default = '765'

    def __init__(self, param):
        if param in self.accepts:
            self.v = param
        else:
            self.v = self.default

    def __repr__(self):
        return self.v


class Song(object):
    _db = df_song
    _primary_key = 'name'

    def __init__(self, name):
        name = str(name)
        df = self._db[self._db[self._primary_key].isin([name])]
        if len(df) != 1:
            InvalidSongModel(
                'Song {0} is not independent'.format(name))

        # 各rowをcolumn名でattrに格納
        for _, row in df.iterrows():
            for column in df.columns:
                setattr(self, column, row[column])

    def __repr__(self):
        return '<{0} {1}>'.format(
            self.__class__.__name__, repr(getattr(self, self._primary_key)))


class Actor(object):
    _db = df_actr
    _relation = df_char2actr

    # attr振り分け用のkey設定
    _primary_key = 'name'
    _relation_key = 'character'
    _date_key = 'start_date'
    _affiliation_key = 'affiliation'

    def __init__(self, name):
        name = str(name)
        df = self._db[self._db[self._primary_key].isin([name])]
        if len(df) != 1:
            raise InvalidActorModel(
                'Actor {0} is not independent'.format(name))

        # 各rowをcolumn名でattrに格納
        for _, row in df.iterrows():
            for column in df.columns:
                setattr(self, column, row[column])

        # 役者に固有な値のinit
        self.characters = []

    def __repr__(self):
        return '<{0} {1}: {2}>'.format(
            self.__class__.__name__, repr(getattr(self, self._primary_key)),
            self.characters)

    def set_characters(self, affiliations, date=None):
        """役者の関係性からフィルターしたキャラクターのリストを格納する。

        :param affiliations: 関係のある所属のリスト, ['765', ...]
        :type affiliations: list
        :param date: 役者がキャラクターに就いた日時, default is None
        :type date: datetime.datetime
        :return: キャラクターのリスト
        :rtype: list
        """

        # 役者とキャラクターの関係を取得
        rel = self._relation[self._relation['actor'].isin(
            [getattr(self, self._primary_key)])]
        if len(affiliations) != 0:
            # 所属でフィルター
            rel = rel[rel[self._affiliation_key].isin(affiliations)]
        if date is not None:
            # 日時でフィルターして関係性を取得
            rel = (rel[~(rel[self._date_key] > date)].sort(
                self._date_key,
                ascending=False))  # 役者が一人二役などをしているかもしれない

        # キャラクターModelを作成
        ms = get_models(rel, Character, [self._relation_key])
        if len(ms) > 1:
            ms = sorted(ms, key=lambda x: x.name_kana)  # 50音順
        elif len(ms) == 0:
            ms = []
            # raise InvalidActorModel(
            #     'Actor {0} has no characters'.format(
            #         getattr(self, self._primary_key)))
        self.characters = ms


class Character(object):
    _db = df_char
    _relation = df_char2actr

    # attr振り分け用のkey設定
    _primary_key = 'name'
    _relation_key = 'actor'
    _date_key = 'start_date'
    _affiliation_key = 'affiliation'

    def __init__(self, name):
        name = str(name)
        df = self._db[self._db[self._primary_key].isin([name])]
        if len(df) != 1:
            raise InvalidCharacterModel(
                'Character {0} is not independent'.format(name))

        # 各rowをcolumn名でattrに格納
        for _, row in df.iterrows():
            for column in df.columns:
                setattr(self, column, row[column])

        # キャラクターに固有な値のinit
        self.actors = []

    def __repr__(self):
        return '<{0} {1}: {2}>'.format(
            self.__class__.__name__, repr(getattr(self, self._primary_key)),
            self.actors)

    def set_actors(self, affiliations, date=None):
        """キャラクターの関係性からフィルターした役者のリストを格納する。

        :param affiliations: 関係のある所属のリスト, ['765', ...]
        :type affiliations: list
        :param date: キャラクターに役者が就いた日時, default is None
        :type date: datetime.datetime
        :return: 役者のリスト
        :rtype: list
        """

        # 役者とキャラクターの関係を取得
        rel = self._relation[self._relation['character'].isin(
            [getattr(self, self._primary_key)])]
        if len(affiliations) != 0:
            # 所属でフィルター
            rel = rel[rel[self._affiliation_key].isin(affiliations)]
        if date is not None:
            # 日時でフィルターして関係性を取得
            rel = (rel[~(rel[self._date_key] > date)].sort(
                self._date_key,
                ascending=False)  # キャラクターに二人以上の役者が就いているかも
                .drop_duplicates('character'))  # 同じ日時なら起こらないはず

        # 役者Modelを作成
        ms = get_models(rel, Actor, [self._relation_key])
        if len(ms) > 1:
            ms = sorted(ms, key=lambda x: x.name_kana)  # 50音順
        elif len(ms) == 0:
            ms = []
            # raise InvalidCharacterModel(
            #     'Character {0} has no actors'.format(
            #             getattr(self, self._primary_key)))
        self.actors = ms


class Event(object):
    _db = df_event
    _relation = df_song2actr2event
    _src = df_slsrc

    # attr振り分け用のkey設定
    _primary_key = 'name'
    _affiliation_key = 'affiliation'
    _part_key = 'part_number'
    _song_key = 'song'
    _order_key = 'order'

    # パートに固有なkeyのlist
    _independent_keys = ['affiliation', 'asin',
                         'amazon_link', 'amazon_small_image_link',
                         'amazon_medium_image_link', 'amazon_large_image_link']

    def __init__(self, name):
        name = str(name)
        df = self._db[self._db[self._primary_key].isin([name])]

        # イベントの出典を辞書のリストとして格納
        url_mask = self._src.columns.isin([c for c in self._src.columns
                                           if 'url' in c])
        self.sources = self._src[self._src['event'].isin(
            [name])].loc[:, url_mask].to_dict('records')

        # イベントのパートに固有な値を辞書のリストとして格納
        part_mask = df.columns.isin(
            [c for c in df.columns if ('part' in c) or
                                      (c in self._independent_keys)])
        self.parts = df.loc[:, part_mask].to_dict('records')

        # イベントの所属をリストに変換
        for p in self.parts:
            p['affiliations'] = [AffiliationType(a.strip()).v for a in p.pop(
                self._affiliation_key).split('|')]
            # パートに固有な値のinit
            p['actors'] = []
            p['setlists'] = []

        # パートに固有でない値(つまり、パートに共通の値)をattrとして格納
        single_row = len(df) == 1
        for column in df.loc[:, ~part_mask].columns:
            if column == 'index':
                continue
            if single_row or df[column].nunique() == 1:
                v = df[column].iloc[0]
                if isinstance(v, Timestamp):
                    v = v.to_datetime()
                setattr(self, column, v)
            else:
                # イベントに固有なはずの値に重複が存在する
                raise InvalidEventModel(
                    'DataFrame column {0} has multiple values'.format(column))

        # イベントに固有な値のinit
        self.actors = []

    def __repr__(self):
        return '<{0} {1} {2}>'.format(
            self.__class__.__name__, repr(getattr(self, self._primary_key)),
            self.actors)

    def set_actors(self, sort=True, sort_by='name_kana'):
        """イベントに含まれる役者のリストを格納する。

        :param sort: Modelのsortを行なうかどうか
        :type sort: bool
        :param sort_by: sortを行なうcolumn名
        :type sort_by: str
        """
        if not getattr(self, 'actors', None):
            relation = self._relation[self._relation['event'] ==
                                      getattr(self, self._primary_key)]

            self.actors = get_relational_actor_models(
                relation, sort=sort, sort_by=sort_by,
                duplicate_columns=['character', 'actor'])

    def set_setlists(self, sort=True, sort_by='name_kana'):
        """イベントに含まれるセットリストとパート毎の役者のリストを格納する。

        :param sort: 役者Modelのsortを行なうかどうか
        :type sort: bool
        :param sort_by: sortを行なうcolumn名
        :type sort_by: str
        """
        relation = self._relation[self._relation['event'] ==
                                  getattr(self, self._primary_key)]

        for p in self.parts:
            # パートのセットリストの関係
            rel = relation[relation[self._part_key] == p[self._part_key]]

            # パートの役者のリストを格納
            p['actors'] = get_relational_actor_models(
                rel, sort=sort, sort_by=sort_by,
                duplicate_columns=['character', 'actor'])

            # パートのセットリストを格納
            setlists = get_models(
                rel, SetList, ['event', self._part_key, self._order_key],
                sort=True, sort_by=self._order_key)
            # セットリストの役者のリストを格納
            [l.set_actors(sort=sort, sort_by=sort_by) for l in setlists]
            p['setlists'] = setlists


class SetList(object):
    _db = df_song2actr2event

    # セットリストに固有なkey
    _primary_key = 'event'
    _secondary_key = 'part_number'
    _tertiary_key = 'order'

    _group_key = 'group'
    _song_key = 'song'
    _order_key = 'order'

    def __init__(self, event, part, order):
        event, part, order = str(event), int(part), int(order)
        df = self._db[(self._db[self._primary_key] == event) &
                      (self._db[self._secondary_key] == part) &
                      (self._db[self._tertiary_key] == order)]

        # イベントの情報を格納
        event = Event(event)
        setattr(self, self._primary_key, event)
        setattr(self, 'part_name', event.parts[part-1]['part_name'])

        # 曲Modelを設定
        self.song = get_model(df, Song, [self._song_key])

        mask = df.columns.isin(
            [c for c in df.columns if
             (c in ['index', 'character', 'actor', self._group_key,
                    self._primary_key, self._song_key])])

        # セットリストに固有な値を格納
        single_row = len(df) == 1
        for column in df.loc[:, ~mask].columns:
                if single_row or df[column].nunique() == 1:
                    v = df[column].iloc[0]
                    if self._song_key in column:
                        # 曲の情報として格納
                        setattr(self.song, column, v)
                    else:
                        setattr(self, column, v)
                else:
                    # セットリストに固有なはずの値に重複が存在する
                    raise InvalidSetListModel(
                        'DataFrame column {0} has multiple values'.format(
                            column))

        # セットリストに固有な値のinit
        self.actors = {}

    def __repr__(self):
        return '<{0} {1} {2} {3}>'.format(
            self.__class__.__name__, getattr(self, self._order_key),
            repr(getattr(self, self._song_key)), self.actors)

    def set_actors(self, sort=True, sort_by='name_kana'):
        """グループで分けられた役者の辞書を格納する。

        :param sort: Modelのsortを行なうかどうか
        :type sort: bool
        :param sort_by: sortを行なうcolumn名
        :type sort_by: str
        """
        db = self._db[(self._db[self._primary_key] ==
                       getattr(self, self._primary_key).name) &
                      (self._db[self._secondary_key] ==
                       getattr(self, self._secondary_key)) &
                      (self._db[self._tertiary_key] ==
                       getattr(self, self._tertiary_key))]

        self.actors = get_relational_actor_models(
            db, sort=sort, sort_by=sort_by,
            duplicate_columns=['character', 'actor', self._group_key])

        return self.actors


class Release(object):
    _db = df_release
    _relation = df_song2char2release
    _src = df_tlsrc

    # attr振り分け用のkey設定
    _primary_key = 'name'
    _affiliation_key = 'affiliation'
    _disc_key = 'disc_number'
    _song_key = 'song'
    _order_key = 'order'

    # ディスクに固有なkeyのlist
    _independent_keys = ['affiliation', 'catalog_number', 'asin',
                         'amazon_link', 'amazon_small_image_link',
                         'amazon_medium_image_link', 'amazon_large_image_link']

    def __init__(self, name):
        name = str(name)
        df = self._db[self._db[self._primary_key].isin([name])]

        # リリースの出典を辞書のリストとして格納
        url_mask = self._src.columns.isin([c for c in self._src.columns
                                           if 'url' in c])
        self.sources = self._src[self._src['release'].isin(
            [name])].loc[:, url_mask].to_dict('records')

        # リリースのディスクに固有な値を辞書のリストとして格納
        disc_mask = df.columns.isin(
            [c for c in df.columns if ('disc' in c) or
                                      (c in self._independent_keys)])
        self.discs = df.loc[:, disc_mask].to_dict('records')

        # ディスクの所属をリストに変換
        for d in self.discs:
            d['affiliations'] = [AffiliationType(c.strip()).v for c in d.pop(
                self._affiliation_key).split('|')]
            # ディスクに固有な値のinit
            d['characters'] = []
            d['tracklists'] = []

        # ディスクに固有でない値(つまり、ディスクに共通の値)をattrとして格納
        single_row = len(df) == 1
        for column in df.loc[:, ~disc_mask].columns:
            if column == 'index':
                continue
            if single_row or df[column].nunique() == 1:
                v = df[column].iloc[0]
                if isinstance(v, Timestamp):
                    v = v.to_datetime()
                setattr(self, column, v)
            else:
                # リリースに固有なはずの値に重複が存在する
                raise InvalidReleaseModel(
                    'DataFrame column {0} has multiple values'.format(column))

        # リリースに固有な値のinit
        self.characters = []

    def __repr__(self):
        return '<{0} {1} {2}>'.format(
            self.__class__.__name__, repr(getattr(self, self._primary_key)),
            self.characters)

    def set_characters(self, sort=True, sort_by='name_kana'):
        """リリースに含まれるキャラクターのリストを格納する。

        :param sort: Modelのsortを行なうかどうか
        :type sort: bool
        :param sort_by: sortを行なうcolumn名
        :type sort_by: str
        """
        if not getattr(self, 'characters', None):
            relation = self._relation[self._relation['release'] ==
                                      getattr(self, self._primary_key)]
            self.characters = get_relational_character_models(
                relation, sort=sort, sort_by=sort_by,
                duplicate_columns=['character', 'actor'])

    def set_tracklists(self, sort=True, sort_by='name_kana'):
        """リリースに含まれるトラックリストとディスク毎のキャラクターのリストを格納する。

        :param sort: キャラクターModelのsortを行なうかどうか
        :type sort: bool
        :param sort_by: sortを行なうcolumn名
        :type sort_by: str
        """
        relation = self._relation[self._relation['release'] ==
                                  getattr(self, self._primary_key)]

        for d in self.discs:
            # ディスクのトラックリストの関係
            rel = relation[relation[self._disc_key] == d[self._disc_key]]

            # ディスクのキャラクターのリストを格納
            d['characters'] = get_relational_character_models(
                rel, sort=sort, sort_by=sort_by,
                duplicate_columns=['character', 'actor'])

            # ディスクのトラックリストを格納
            tracklists = get_models(
                rel, TrackList, ['release', self._disc_key, self._order_key],
                sort=True, sort_by=self._order_key)
            # トラックリストのキャラクターのリストを格納
            [l.set_characters(sort=sort, sort_by=sort_by) for l in tracklists]
            d['tracklists'] = tracklists


class TrackList(object):
    _db = df_song2char2release

    # トラックリストに固有なkey
    _primary_key = 'release'
    _secondary_key = 'disc_number'
    _tertiary_key = 'order'

    _group_key = 'group'
    _song_key = 'song'
    _order_key = 'order'

    def __init__(self, release, disc, order):
        release, disc, order = str(release), int(disc), int(order)
        df = self._db[(self._db[self._primary_key] == release) &
                      (self._db[self._secondary_key] == disc) &
                      (self._db[self._tertiary_key] == order)]

        # リリースの情報を格納
        release = Release(release)
        setattr(self, self._primary_key, release)
        setattr(self, 'disc_name', release.discs[disc-1]['disc_name'])

        # 曲Modelを設定
        self.song = get_model(df, Song, [self._song_key])

        mask = df.columns.isin(
            [c for c in df.columns if
             (c in ['index', 'character', 'actor', self._group_key,
                    self._primary_key, self._song_key])])

        # トラックリストに固有な値を格納
        single_row = len(df) == 1
        for column in df.loc[:, ~mask].columns:
                if single_row or df[column].nunique() == 1:
                    v = df[column].iloc[0]
                    if self._song_key in column:
                        # 曲の情報として格納
                        setattr(self.song, column, v)
                    else:
                        setattr(self, column, v)
                else:
                    # トラックリストに固有なはずの値に重複が存在する
                    raise InvalidTrackListModel(
                        'DataFrame column {0} has multiple values'.format(
                            column))

        # トラックリストに固有な値のinit
        self.characters = {}

    def __repr__(self):
        return '<{0} {1} {2} {3}>'.format(
            self.__class__.__name__, getattr(self, self._order_key),
            repr(getattr(self, self._song_key)), self.characters)

    def set_characters(self, sort=True, sort_by='name_kana'):
        """グループで分けられたキャラクターの辞書を格納する。

        :param sort: Modelのsortを行なうかどうか
        :type sort: bool
        :param sort_by: sortを行なうcolumn名
        :type sort_by: str
        """
        db = self._db[(self._db[self._primary_key] ==
                       getattr(self, self._primary_key).name) &
                      (self._db[self._secondary_key] ==
                       getattr(self, self._secondary_key)) &
                      (self._db[self._tertiary_key] ==
                       getattr(self, self._tertiary_key))]

        self.characters = get_relational_character_models(
            db, sort=sort, sort_by=sort_by,
            duplicate_columns=['character', 'actor', self._group_key])

        return self.characters
