# -*- coding: utf-8 -*-

"""
setori.api
~~~~~~~~~~

This module contains the set of setori's api.

"""

from flask import Blueprint, request, json, Response
from . import df_song, df_actr, df_char
from .models import search_models, Song, Actor, Character

api = Blueprint('api', __name__, url_prefix='/api/v1')


def get_valued_tokens(models):
    ls = []
    for model in models:
        d = {}

        if hasattr(model, 'alias'):
            d['value'] = model.name if (
                getattr(model, 'alias') == '') else model.alias
        else:
            d['value'] = model.name
        d['tokens'] = [v for k, v in model.__dict__.items() if 'name' in k]
        ls.append(d)
    return ls


@api.route('/songs', methods=['GET'])
def api_songs():
    ms = search_models(request.args.get('q', ''), df_song, Song)
    return Response(json.dumps(get_valued_tokens(ms), ensure_ascii=False),
                    mimetype='application/json')


@api.route('/actors', methods=['GET'])
def api_actors():
    ms = search_models(request.args.get('q', ''), df_actr, Actor)
    return Response(json.dumps(get_valued_tokens(ms), ensure_ascii=False),
                    mimetype='application/json')


@api.route('/characters', methods=['GET'])
def api_characters():
    ms = search_models(request.args.get('q', ''), df_char, Character)
    return Response(json.dumps(get_valued_tokens(ms), ensure_ascii=False),
                    mimetype='application/json')
