# -*- coding: utf-8 -*-

# ┌─┐┌─┐┌┬┐┌─┐┬─┐┬
# └─┐├┤    │  │  │├┬┘│
# └─┘└─┘  ┴  └─┘┴└─┴

"""
Setori management library
~~~~~~~~~~~~~~~~~~~~~~~~~

Setori is a library that manages a content which have songs,
actors, characters, events and releases.

"""

from app import engine
from .exceptions import TableNotFound
import pandas as pd

__title__ = 'setori'
__version__ = '0.0.1'
__author__ = 'hironow'
__license__ = ''
__copyright__ = 'Copyright 2015 hironow'

tables = ['char', 'actr', 'char2actr', 'song',
          'event', 'song2actr2event', 'slsrc',
          'release', 'song2char2release', 'tlsrc']
for table in tables:
    if not engine.has_table(table):
        raise TableNotFound('Table %s not found' % table)

# Get database as pandas.DataFrame
df_char = pd.read_sql_table('char', engine)
df_actr = pd.read_sql_table('actr', engine)
df_char2actr = pd.read_sql_table('char2actr', engine)

df_song = pd.read_sql_table('song', engine)

df_event = pd.read_sql_table('event', engine)
df_song2actr2event = pd.read_sql_table('song2actr2event', engine)
df_slsrc = pd.read_sql_table('slsrc', engine)

df_release = pd.read_sql_table('release', engine)
df_song2char2release = pd.read_sql_table('song2char2release', engine)
df_tlsrc = pd.read_sql_table('tlsrc', engine)

# Convert datetime
df_char2actr['start_date'] = pd.to_datetime(df_char2actr['start_date'])
df_event['date'] = pd.to_datetime(df_event['date'])
df_release['date'] = pd.to_datetime(df_release['date'])

# Drop not using columns
if 'level_0' in df_song2actr2event.columns:
    df_song2actr2event.drop('level_0', axis=1, inplace=True)
if 'level_0' in df_song2char2release.columns:
    df_song2char2release.drop('level_0', axis=1, inplace=True)
